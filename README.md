# Hackathon-2022 Freiplatzbörse für Sportangebote

## Documents for the Project

### Shared cloud space
https://drive.google.com/drive/folders/1HOl3CGtwDuyi6ZLZM1enr1S_PmZfqsih?usp=sharing

| Document               | Link                                                                                             |
|------------------------|--------------------------------------------------------------------------------------------------|
| Geschäftsmodell        | https://docs.google.com/document/d/1w1-zjKl24uUAhItNjirMA6FSfZ3eGsLbk5RAO8HQioI/edit?usp=sharing |
| Marketing Page Konzept | https://docs.google.com/document/d/1f8i9RmbqGO22AuPGe1_2yPmDemh4nYIsmUg-ymA4s7A/edit?usp=sharing |

## Dev

### Frontend

Single Page Application via vue.js – Bootstrap 5 for Frontend – CSS via SASS

### Backend

Mssql as Database – .NET for rest api

### Apache Cordova to run the Application Native on iOS and Android

https://cordova.apache.org/