﻿namespace Sportify.Models
{
    public class Provider
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string RegulationURL { get; set; }
    }
}
