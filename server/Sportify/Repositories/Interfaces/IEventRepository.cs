﻿using Sportify.Models;

namespace Sportify.Repositories.Interfaces
{
    public interface IEventRepository
    {
        List<Event> GetAll();
        void Save(Event courseEvent);
    }
}
