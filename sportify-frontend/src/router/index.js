import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/dashboard",
      name: "dashboard",
      component: () => import("../views/DashboardView.vue"),
    },
    {
      path: "/activities",
      name: "activities",
      component: () => import("../views/ActivityView.vue"),
    },
    {
      path: "/register",
      name: "register",
      component: () => import("../views/RegisterView.vue"),
    },
    {
      path: "/login",
      name: "login",
      component: () => import("../views/LoginView.vue"),
    },
    {
      path: "/logout",
      name: "logout",
      component: () => import("../views/LogoutView.vue"),
    },
    {
      path: "/activities/view",
      name: "viewActivity",
      component: () => import("../views/activities/ViewActivityView.vue"),
    },
    {
      path: "/activities/create",
      name: "createActivity",
      component: () => import("../views/activities/CreateActivityView.vue"),
    },
    {
      path: "/activities/edit",
      name: "editActivity",
      component: () => import("../views/activities/EditActivityView.vue"),
    },
    {
      path: "/activities/confirm",
      name: "confirmActivity",
      component: () => import("../views/activities/ConfirmActivityView.vue"),
    },
  ],
});

export default router;
